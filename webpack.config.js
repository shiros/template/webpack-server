/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2021
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : webpack.config.js
 * @Created_at  : 12/02/2021
 * @Update_at   : 13/02/2021
 * ----------------------------------------------------------------
 */

// Require needed libraries
const path = require('path');
const webpack = require('webpack');

// Require needed configuration
const pkg = require("./package.json");

// Require needed plugins
const {CleanWebpackPlugin} = require('clean-webpack-plugin');

// Webpack configuration
module.exports = {
    // --------------------------------
    // General Settings

    // Mode
    mode: 'development',

    // Library entry point
    entry: path.resolve(__dirname, './src/main.js'),

    // Library output
    output: {
        path: path.resolve(__dirname, './public/assets'),
        publicPath: '/assets/',
        filename: '[name].js',

        library: {
            name: pkg.name,
            type: "umd"
        }
    },

    // Options
    devtool: 'eval-source-map',

    // Development Server
    devServer: {
        host: 'localhost',
        port: 8080,

        hot: true,
        overlay: true,
        compress: true,

        // writeToDisk: true,
        watchContentBase: true,
        historyApiFallback: true,

        contentBase: path.resolve(__dirname, './public')
    },

    // --------------------------------
    // Build Settings

    // File resolver
    resolve: {
        alias: {
            '@': path.resolve(__dirname, './src')
        },
        modules: [
            path.resolve('./node_modules'),
            path.resolve('./src')
        ],
        extensions: ['*', '.js', '.json']
    },

    // Modules
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: ['babel-loader', 'eslint-loader']
            }
        ]
    },

    // Plugins
    plugins: [
        new webpack.ProgressPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new CleanWebpackPlugin()
    ]
};
